/*
 *  mmult.c: matrix multiplication using MPI.
 * There are some simplifications here. The main one is that matrices B and C
 * are fully allocated everywhere, even though only a portion of them is
 * used by each processor (except for processor 0)
 */

#include <mpi.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define SIZE 1024                        /*Size of matrices */

//int SIZE;
int A[SIZE][SIZE], B[SIZE][SIZE], C[SIZE][SIZE];

void fill_matrix(int m[SIZE][SIZE])
{
  static int n=0;
  int i, j;
  for (i=0; i<SIZE; i++)
    for (j=0; j<SIZE; j++)
      m[i][j] = n++;
}

void print_matrix(int m[SIZE][SIZE])
{
  int i, j = 0;
  for (i=0; i<SIZE; i++) {
    printf("\n\t| ");
    for (j=0; j<SIZE; j++)
      printf("%2d ", m[i][j]);
    printf("|");
  }
}


int main(int argc, char *argv[])
{
  int myrank, P, from, to, i, j, k;
  int sz;
  int tag = 666;                /* any value will do */
  MPI_Status status;

  MPI_Init (&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);       /* who am i */
  MPI_Comm_size(MPI_COMM_WORLD, &P); /* number of processors */

//  scanf("Tuliskan ukuran matriks nxn: %d", &sz);
  //SIZE = sz;
  /* Just to use the simple variants of MPI_Gather and MPI_Scatter we */
  /* impose that SIZE is divisible by P. By using the vector versions, */
  /* (MPI_Gatherv and MPI_Scatterv) it is easy to drop this restriction. */

  if (SIZE%P!=0) {
    if (myrank==0) printf("Matrix size not divisible by number of processors\n");
    MPI_Finalize();
    exit(-1);
  }

  from = myrank * SIZE/P;
  to = (myrank+1) * SIZE/P;

  /* Process 0 fills the input matrices and broadcasts them to the rest */
  /* (actually, only the relevant stripe of A is sent to each process) */

  if (myrank==0) {
    fill_matrix(A);
    fill_matrix(B);
  }

  double tscom1 = MPI_Wtime();
  MPI_Bcast (B, SIZE*SIZE, MPI_INT, 0, MPI_COMM_WORLD);

  if(myrank==0){
    MPI_Scatter (&A[0][0], SIZE*SIZE/P, MPI_INT, MPI_IN_PLACE, SIZE*SIZE/P, MPI_INT, 0, MPI_COMM_WORLD);
  }else{
    MPI_Scatter (&A[0][0], SIZE*SIZE/P, MPI_INT, &A[from][0], SIZE*SIZE/P, MPI_INT, 0, MPI_COMM_WORLD);
  }

  double tfcom1 = MPI_Wtime();

  printf("computing slice %d (from row %d to %d)\n", myrank, from, to-1);
  double tsop = MPI_Wtime();
  for (i=from; i<to; i++)
    for (j=0; j<SIZE; j++) {
      C[i][j]=0;
      for (k=0; k<SIZE; k++)
        C[i][j] += A[i][k]*B[k][j];
    }

  double tfop = MPI_Wtime();
  double tscom2 = MPI_Wtime();

  if(myrank==0){
    MPI_Gather (MPI_IN_PLACE, SIZE*SIZE/P, MPI_INT, &C[0][0], SIZE*SIZE/P, MPI_INT, 0, MPI_COMM_WORLD);
  }else{
    MPI_Gather (&C[from][0], SIZE*SIZE/P, MPI_INT, &C[0][0], SIZE*SIZE/P, MPI_INT, 0, MPI_COMM_WORLD);
  }

  double tfcom2 = MPI_Wtime();

  if (myrank==0) {
    float com_time = (tfcom1-tscom1) + (tfcom2-tscom2);
    float ops_time = tfop - tsop;
    float total_time = com_time + ops_time;

    printf("\n\n");
    print_matrix(A);
    printf("\n\n\t       * \n");
    print_matrix(B);
    printf("\n\n\t       = \n");
    print_matrix(C);
    printf("\n\n");

    printf("Communication time: %f\n", com_time);
    printf("Operations time: %f\n", ops_time);
    printf("Total time: %f\n", total_time);
  }

  MPI_Finalize();
  return 0;
}
