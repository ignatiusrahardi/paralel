#include "mpi.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

int main(int argc, char **argv)
{
  // Initialize the MPI environment
  MPI_Init(NULL, NULL);

  // Get the rank and size in the original communicator
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  // Get the group of processes in MPI_COMM_WORLD
  MPI_Group world_group;
  MPI_Comm_group(MPI_COMM_WORLD, &world_group);

  int n = 6;
  const int prime_num[6] = {2, 3, 5, 7, 11, 13};
  const int even_num[7] = {0, 2, 4, 6, 8, 10, 12};

  // Construct a group containing all of the prime ranks in world_group
  MPI_Group prime_group, even_group, union_group, intersection_group;
  MPI_Group_incl(world_group, 6, prime_num, &prime_group);
  MPI_Group_incl(world_group, 7, even_num, &even_group);
  MPI_Group_union(prime_group, even_group, &union_group);
  MPI_Group_intersection(prime_group, even_group, &intersection_group);

  // Create a new communicator based on the group
  MPI_Comm prime_comm, even_comm, union_comm, intersection_comm;
  MPI_Comm_create_group(MPI_COMM_WORLD, prime_group, 0, &prime_comm);
  MPI_Comm_create_group(MPI_COMM_WORLD, even_group, 0, &even_comm);
  MPI_Comm_create_group(MPI_COMM_WORLD, union_group, 0, &union_comm);
  MPI_Comm_create_group(MPI_COMM_WORLD, intersection_group, 0, &intersection_comm);

  // Get the name of the processor
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int name_len;
  MPI_Get_processor_name(processor_name, &name_len);

    printf("Hello world from processor %s, rank %d. ", processor_name, world_rank);

  if (MPI_COMM_NULL != union_comm) {
    printf("I'm in a group! ");
  } else {
    printf("I'm not in a group.\n");
  }

  if (MPI_COMM_NULL != intersection_comm) {
    printf("My rank is an even number and a prime number.\n");
  } else if (MPI_COMM_NULL != prime_comm) {
    printf("My rank is a prime number.\n");
  } else if (MPI_COMM_NULL != even_comm) {
    printf("My rank is an even number.\n");
  }

  // Finalize the MPI environment.
  MPI_Finalize();
}
