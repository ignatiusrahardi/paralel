#include "mpi.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

int main(int argc, char **argv)
{
  // Initialize the MPI environment
  MPI_Init(NULL, NULL);

  int dims[2], periods[2], wsize;
  int topo_type, rank, size;
  int coord[2], id;
  MPI_Comm cartcomm, cartcomm2;

  MPI_Comm_size(MPI_COMM_WORLD, &wsize);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // Create cartesian (mesh) topology
  dims[0] = dims[1] = 0;
  MPI_Dims_create(wsize, 2, dims);

  periods[0] = periods[1] = 0;
  MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 0, &cartcomm);

  int ndim;
  MPI_Cartdim_get(cartcomm, &ndim);
  printf( "The dimension of the topology is %d\n", ndim );
  fflush(stdout);

  // Duplicate communicator and get topology type
  MPI_Comm_dup(cartcomm, &cartcomm2);
  MPI_Topo_test(cartcomm2, &topo_type);

  int ndim2;
  MPI_Cartdim_get(cartcomm2, &ndim2);
  printf( "The dimension of the dup topology is %d\n", ndim2 );
  fflush(stdout);

  MPI_Cart_coords(cartcomm, rank, 2, coord);
  printf( "Rank %d coordinates are %d %d\n", rank, coord[0], coord[1] );
  fflush(stdout);

  int errs = 0, i;
  int outdims[2], outperiods[2], outcoords[2];
  if (topo_type != MPI_CART)
  {
    errs++;
    printf( " Topo type of duped cart was not cart\n" );
    fflush(stdout);
  }
  else
  {
    printf( "Topo type of duped cart was cart\n" );
    fflush(stdout);

    MPI_Cart_get(cartcomm2, 2, outdims, outperiods, outcoords);

    // Checks the information from original communicator with the duplicate
    for (i = 0; i < 2; i++)
    {
      if (outdims[i] == dims[i])
      {
        printf( "%d = outdims[%d] == dims[%d] = %d\n", outdims[i], i, i, dims[i] );
        fflush(stdout);
      }
      else
      {
        errs++;
        printf( "%d = outdims[%d] != dims[%d] = %d\n", outdims[i], i, i, dims[i] );
        fflush(stdout);
      }

      if (outperiods[i] == periods[i])
      {
        printf( "%d = outperiods[%d] == periods[%d] = %d\n", outperiods[i], i, i, periods[i] );
        fflush(stdout);
      }
      else
      {
        errs++;
        printf( "%d = outperiods[%d] != periods[%d] = %d\n", outperiods[i], i, i, periods[i] );
        fflush(stdout);
      }

      printf( "Number of errors are %d\n", errs );
      fflush(stdout);
    }
  }

  // Communication in topology
  int source, dest;
  MPI_Cart_shift( cartcomm, 0, coord[1], &source, &dest );
  printf( "source for shift 1 is %d\n", source );fflush(stdout);
  printf( "dest for shift 1 is %d\n", dest );fflush(stdout);
  // if (source != ((rank - 1 + size) % size)) {
  //   errs++;
  //   printf( "source for shift 1 is %d\n", source );fflush(stdout);
  // }
  // if (dest != ((rank + 1) % size)) {
  //   errs++;
  //   printf( "dest for shift 1 is %d\n", dest );fflush(stdout);
  // }


  // Destroy the communicators
  MPI_Comm_free(&cartcomm2);
  MPI_Comm_free(&cartcomm);

  // Finalize the MPI environment.
  MPI_Finalize();
}
